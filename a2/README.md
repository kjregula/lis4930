> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930-0002 - Special Topics: Advanced Mobile Web Application Development

## Kevin Regula

### Assignment #2 Requirements:

*Three Parts:*

1. Tip Calculator
2. Chapter Questions (Chs 3, 4)
3. Bitbucket repo links: a) this assignment

#### README.md file should include the following items:

* Course title, name, assignment requirements, as per A1
* Screenshot of running application's unpopulated user interface
* Screenshot of running application's populated user interface

#### Assignment Screenshots:

*Screenshot of Tip Calculator unpopulated user interface*:

![Tip Calculator unpopulated interface](img/unpopulated.png)

*Screenshot of Tip Calculator populated user interface*:

![Tip Calculator populated interface](img/populated.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
