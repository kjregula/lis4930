> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930-0002 - Special Topics: Advanced Mobile Web Application Development

## Kevin Regula

### Assignment #5 Requirements:

*Three Parts:*
1. News Reader
2. Bitbucket repo links: a) this assignment

#### README.md file should include the following items:
* Course title, name, assignment requirements, as per A4
* Screenshot of running application's splash screen
* Screenshot of running application's individual screen
* Screenshot of running application's default browser screen

#### Assignment Screenshots:
| *Screenshot of News Reader's splash screen*: | *Screenshot of News Reader's individual screen*: | *Screenshot of News Reader's default browser screen*: |
| --------------------------------------------:|:----------------------------------------------------:|:----------------------------------------------------- |
| ![News Reader splash screen](img/splash.png) | ![News Reader individual screen](img/individual.png) | ![News Reader default browser screen](img/default.png)|