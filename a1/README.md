> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930-0002 - Special Topics: Advanced Mobile Web Application Development

## Kevin Regula

### Assignment #1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1, 2)
4. Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes).

#### *README.md file should include the following items:*

* Screenshot of running java Hello
* Screen shot of running Android Studio - My First App
* Screenshots of running Android Studio - Contacts App
* git commands w/ short descriptions

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### *Git commands w/short descriptions:*

1. git init - Create an empty Git repository or reinitialize an existing 
one

2. git status - Show the working tree status

3. git add - Add file contents to the index

4. git commit - Record changes to the repository

5. git push - Update remote refs along with associated objects

6. git pull - Fetch from and integrate with another repository or a local branch

7. git grep - Print lines matching a pattern


#### Assignment Screenshots:

> #### *Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

> #### *Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)

> ####*Screenshots of Android Studio - Contacts App*

![Android Studio Contacts Screenshot](img/contacts.png)

![Android Studio Contact Screenshot](img/contact1.png)

![Android Studio Contact Screenshot](img/contact2.png)

![Android Studio Contact Screenshot](img/contact3.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
