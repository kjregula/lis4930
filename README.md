> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930-0002 - Special Topics: Advanced Mobile Web Application Development

## Kevin Regula

### LIS4930 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md)
    * Install JDK
    * Install Android Studio and create My First App and Contacts App
    * Provide screenshots of installations
    * Create Bitbucket repo
    * Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    * Provide git command descriptions
2. [A2 README.md](a2/README.md)
    * Create Tip Calculator Android app
    * Provide screenshots of completed app
3. [A3 README.md](a3/README.md)
    * Create Currency Converter Android app
    * Provide screenshots of completed app
4. [A4 README.md](a4/README.md)
    * Create Home Mortgage Interest Calculator app
    * Provide screenshots of completed app
5. [A5 README.md](a5/README.md)
    * Create News Reader app
    * Provide screenshots of completed app
6. [P1 README.md](p1/README.md)
    * Create Android Media Player app
    * Provide screenshots of completed app
7. [P2 README.md](p2/README.md)
    * Create Android Task List app
    * Provide screenshots of completed app


