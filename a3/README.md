> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930-0002 - Special Topics: Advanced Mobile Web Application Development

## Kevin Regula

### Assignment #3 Requirements:

*Three Parts:*

1. Currency Converter
2. Chapter Questions (Ch 4)
3. Bitbucket repo links: a) this assignment

#### README.md file should include the following items:

* Course title, name, assignment requirements, as per A2
* Screenshot of running application's unpopulated user interface
* Screenshot of running application's toast pop-up
* Screenshot of running application's populated user interface

#### Assignment Screenshots:

*Screenshot of Currency Converter unpopulated user interface*:

![Currency Converter unpopulated interface](img/unpopulated.png)

*Screenshot of Currency Converter toast pop-up*

![Currency Converter toast pop-up](img/toast.png)

*Screenshot of Currency Converter populated user interface*:

![Currency Converter populated interface](img/populated.png)