> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930-0002 - Special Topics: Advanced Mobile Web Application Development

## Kevin Regula

### Assignment #4 Requirements:

*Three Parts:*
1. House Mortgage Interest Calculator 
2. Chapter Questions (Ch. 11) 
3. Bitbucket repo links: a) this assignment

#### README.md file should include the following items:
* Course title, name, assignment requirements, as per P1
* Screenshot of running application's splash screen
* Screenshot of running application's invalid screen
* Screenshot of running application's valid screen

#### Assignment Screenshots:
| *Screenshot of Interest Calculator's splash screen*: | *Screenshot of Interest Calculator's main screen*: | *Screenshot of Interest Calculator's invalid screen*: | *Screenshot of Interest Calculator's valid screen*: |
| ----------------------------------------------------:|:--------------------------------------------------:|:-----------------------------------------------------:|:----------------------------------------------------|
| ![Interest Calculator splash screen](img/splash.png) | ![Interest Calculator main screen](img/main.png)   | ![Interest Calculator invalid screen](img/invalid.png)| ![Interest Calculator valid screen](img/valid.png)  |