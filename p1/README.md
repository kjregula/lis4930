> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930-0002 - Special Topics: Advanced Mobile Web Application Development

## Kevin Regula

### Project #1 Requirements:

*Three Parts:*
1. Android Media Player 
2. Chapter Questions (Ch 5,6) 
3. Bitbucket repo links: a) this assignment

#### README.md file should include the following items:
* Course title, name, assignment requirements, as per A3
* Screenshot of running application's splash screen
* Screenshot of running application's follow-up screen
* Screenshot of running application's play and pause user interfaces

#### Assignment Screenshots:
| *Screenshot of Media Player's splash screen*: | *Screenshot of Media Player's follow-up screen*: | *Screenshot of Media Player's play and pause user interfaces*: |
| --------------------------------------------- |:------------------------------------------------:| --------------------------------------------------------------:|
| ![Media Player splash screen](img/splash.png) | ![Media Player follow-up screen](img/main.png)   | ![Media Player play and pause user interface](img/paused.png)  |