> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4930-0002 - Special Topics: Advanced Mobile Web Application Development

## Kevin Regula

### Project #2 Requirements:

*Three Parts:*

1. Android Task List
2. Chapter Questions 
3. Bitbucket repo links: a) this assignment

#### README.md file should include the following items:

* Course title, name, assignment requirements, as per P1
* Screenshot of running application's splash screen
* Screenshot of running application's task list screen

#### Assignment Screenshots:
| *Screenshot of Task List's splash screen*: | *Screenshot of Task List's follow-up screen*: |
|:------------------------------------------ | ---------------------------------------------:|
| ![Task List splash screen](img/splash.png) | ![Task List follow-up screen](img/task_list.png)   |